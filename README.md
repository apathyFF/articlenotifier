[![License: GPL v3](https://img.shields.io/badge/License-GPL%20v3-blue.svg)](http://www.gnu.org/licenses/gpl-3.0)
# [AP] New Article Notifications in Chat

This simple addon extends Siropu Chat 2 with the ability to send "New article" notifications to the chat.