<?php

/**
 *  ▄▄▄·  ▄▄▄· ▄▄▄· ▄▄▄▄▄ ▄ .▄ ▄· ▄▌
 * ▐█ ▀█ ▐█ ▄█▐█ ▀█ •██  ██▪▐█▐█▪██▌
 * ▄█▀▀█  ██▀·▄█▀▀█  ▐█.▪██▀▐█▐█▌▐█▪
 * ▐█ ▪▐▌▐█▪·•▐█ ▪▐▌ ▐█▌·██▌▐▀ ▐█▀·.
 *  ▀  ▀ .▀    ▀  ▀  ▀▀▀ ▀▀▀ ·  ▀ •
 *  <https://fortreeforums.xyz/>
 *  Licensed under GPL-3.0-or-later 2021
 *
 *  This file is part of [AP] New Article Notifications in Chat ("ArticleNotifier").
 *
 *  ArticleNotifier is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  ArticleNotifier is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with ArticleNotifier.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace apathy\ArticleNotifier;

use XF;
use XF\Mvc\Entity\Entity;

class Listener
{
    public static function articleEntityPostSave(Entity $entity)
    {
        $app = XF::app();
        $options = XF::options();
        $router = $app->router('public');

        $activityRoomId = $options->apAnAMSActivityRoom;
        $messageType = 'article';

        if($options->apAnAMSActivityArticle && $entity->isInsert()
        && $entity->article_state == 'visible' || $entity->isUpdate()
        && $entity->article_state == 'visible' 
        && $entity->getPreviousValue('article_state') == 'moderated')
        {
            $messageText = XF::phrase('ap_an_new_article_notification', [
                'user' => new XF\PreEscaped(
                    "[USER={$entity->user_id}]{$entity->username}[/USER]"
                ),
                'article' => new XF\PreEscaped(
                    "[URL={$router->buildLink('full:ams', $entity)}]{$entity->title}[/URL]"
                ),
                'category' => new XF\PreEscaped(
                    "[URL={$router->buildLink('full:ams/categories', $entity->Category)}]{$entity->Category->title}[/URL]"
                )
            ]);

            $typeId = $entity->article_id;

            $message = $app->service(
                'Siropu\Chat:Message\Creator', $entity, $activityRoomId, $messageType
            );

            $message->setText($messageText);
            $message->setBotName($options->apAnAMSActivityBotName);
            $message->setTypeId($typeId);
            $message->setTypeCategoryId($entity->category_id);

            $message->save();
        }
        
        if($entity->isChanged('article_state') && $entity->article_state == 'deleted')
        {
            self::deleteMessagesByTypeId($messageType, $typeId);
        }
    }
    
    public static function articleEntityPostDelete(Entity $entity)
    {
        self::deleteMessagesByTypeId('article', $entity->article_id);
    }
     
    private static function deleteMessagesByTypeId($type, $id)
    {
        $delete = false;

        if($type == 'article' && XF::options()->apAnAMSActivityRoom)
        {
            $delete = true;
        }

        if($delete)
        {
            XF::repository('Siropu\Chat:Message')->deleteMessagesByTypeId($type, $id);
        }
    }
}
